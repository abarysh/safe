NOTE
====

As of early 2017, the MATLAB implementation of SAFE is only maintained for legacy purposes. New work related to SAFE is moving to a **Python** package (safepy) and the development version is stored at <https://github.com/baryshnikova-lab/safepy>.


CONTENTS OF THIS FILE
=====================

* Introduction
* Software requirements
* Setup
* Input file formats
* Output file formats
* Examples
* Description of the algorithm
* Help
* How to cite
* Licensing


INTRODUCTION
============

SAFE (or Spatial Analysis of Functional Enrichment) is an automated network annotation algorithm. Given a biological network and a set of functional groups or quantitative features of interest, SAFE performs local enrichment analysis to determine which regions of the network are over-represented for each group of feature. SAFE visualizes the network and maps the detected enrichments onto the network using distinctive colors.

SAFE is implemented in MATLAB. However, a Mac OS X application **SAFE.app** that does not require MATLAB is also available. Please check the [Downloads](https://bitbucket.org/abarysh/safe/downloads "Bitbucket Downloads") page for the most recent release.


SOFTWARE REQUIREMENTS
=====================

For running the basic SAFE analysis (e.g., examples 1 and 2): 

* MATLAB, Bioinformatics Toolbox, Statistics and Machine Learning Toolbox

For running the Cytoscape implementation of the Kamada-Kawai network layout (aka the spring-embedded network layout): 

* Option 1. Java 8 + Cytoscape 3.2 + cyREST app (<https://github.com/idekerlab/cyREST/wiki>)
* Option 2. Java 8 + Cytoscape 3.3 (or later)

These additional functions and toolboxes are packaged with the SAFE code (in accordance with their licensing statements):  

* **inifile.m** for reading INI settings files: <http://www.mathworks.com/matlabcentral/fileexchange/2976-inifile>  
* **MatlabBGL**, implemented by David Gleich, for network analysis: <https://github.com/dgleich/matlab-bgl>  
* **Hartigan's dip test statistic for unimodality**, implemented in MATLAB by Ferenc Mechler: <http://www.nicprice.net/diptest/>  
* **patchline**, implemented by Brett Shoelson, for plotting large-scale networks: <http://www.mathworks.com/matlabcentral/fileexchange/36953-patchline>  
* **jsonlab** for encoding/decoding JSON files: <http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files>
* **urlread2**, implemented by Jim Hokanson, for interacting with CyREST using the PUT request method: <http://www.mathworks.com/matlabcentral/fileexchange/35693-urlread2>


SETUP
=====

1. Place the code anywhere on your computer. For example, in `/AAA/BBB/safe/` (so that the main `safe.m` file is in `/AAA/BBB/safe/safe.m`).

2. Open MATLAB and add the `safe/` folder to the MATLAB path by executing the command:

	`addpath(genpath('/AAA/BBB/safe/'));`
	
3. Create a folder that would store the SAFE results. For example: 

	`mkdir('/CCC/DDD/');`.

4. Make a copy of the SAFE settings file `safe.ini` (located in `/AAA/BBB/safe/`) and save it in the folder that will store the SAFE results (`/CCC/DDD/safe.ini`). Don't change the name of the file, just its location. Also, don't remove the original file, it may be useful in the future as a reference for the default parameters.

5. Open the `/CCC/DDD/safe.ini` file in any text editor and adjust the input/output parameters as needed (see **Description of the algorithm** and the `safe.ini` file itself for a more detailed explanation of the parameters).

6. Open the SAFE main file `/AAA/BBB/safe/safe.m` and set the `path_to_results_folder` variable with the absolute path to the results folder:

	`path_to_results_folder = '/CCC/DDD/';`
	
7. Run `safe.m` (all together as a script or section-by-section to get a closer look at the intermediate steps).

8. The results will be visually displayed in MATLAB, as well as stored in `/CCC/DDD/`.


INPUT FILE FORMATS
==================

Network file
------------
The network can be provided in one of four formats:

1. Cytoscape session file (*.cys). An example is provided in `safe/data/Costanzo_Science_2010.cys`.

	*Note*: The Cytoscape network must have a node attribute named "ORF" that contains the labels intended for matching the functional annotation file. E.g., if the annotation file lists annotations by their gene IDs, the Cytoscape network must have a node attribute  "ORF" that contains the gene IDs of all nodes in the network.

2. 3-column tab-delimited list with the following columns (an example is provided in `safe/data/Costanzo_Science_2010_3cols.txt`): 
 
	* Node label 1
	* Node label 2
	* Edge weight
	
	*Note*: SAFE will use all edges with weights > 0.
	
3. 5-column tab-delimited list with the following columns (an example is provided in `safe/data/Costanzo_Science_2010_5cols.txt`):  
	
	* Node label 1
	* Node systematic name 1
	* Node label 2
	* Node systematic name 2
	* Edge weight
	
	*Note*: SAFE will use all edges with weights > 0.
	
4. Adjacency matrix with 1 row header (node labels) and 1 column header (node labels). An example is provided in `safe/data/Costanzo_Science_2010_adjacencyMatrix.txt`.


Functional annotation file
--------------------------
The functional annotation standard must be provided in a matrix format, where rows correspond to node labels and columns correspond to functional attributes. 

Annotation values can be either binary or quantitative. A sample binary annotation file is provided in `safe/data/sample_annotation_file.txt`. A sample quantitative annotation file is provided in `safe/data/hoepfner_movva_2014_doxorubucin.txt`.


OUTPUT FILE FORMATS
===================

In addition to figures, saved as PDF files, SAFE also prints out 3 data files containing information about functional domains, their associated attributes and enriched neighborhoods. All files are saved in the safe-[date]-[time] subfolder in the SAFE results folder (see below for examples).

domain_properties_annotation-(highest|lowest).txt
-------------------------------------------------
This file lists the properties of all functional domains detected in the network. The 'highest' and 'lowest' files contain the results of the analysis performed when the highest or the lowest values of the attributes are considered for enrichment.

The columns are:

* Domain ID: the number used to point to the domain on the composite map
* Domain name: the 5 words encountered most frequently in the names of the functional attributes associated with the domain
* RGB value: the color used to represent the domain on the composite map
	
attribute_properties_annotation-(highest|lowest).txt
----------------------------------------------------
This file lists the properties of all attributes used to annotate the network. The 'highest' and 'lowest' files contain the results of the analysis performed when the highest or the lowest values of the attributes are considered for enrichment.

The columns are:

* Attribute ID: GO ID (e.g., GO:0010033) for GO-based analyses or a serial unique integer for all other cases
* Attribute name
* Domain ID
	
node_properties_annotation-(highest|lowest).txt
-----------------------------------------------
This file lists the properties of all nodes in the network. The 'highest' and 'lowest' files contain the results of the analysis performed when the highest or the lowest values of the attributes are considered for enrichment.

The columns are:

* Node label
* Node label ORF: the labels that is used to match the functional attribute annotations in the functional annotation file.
* Domain (predominant): the ID of the domain for which this node's neighborhood is enriched the most
* Neighborhood score [max=1, min=0] (predominant): the enrichment score (between 0 and 1) of this node's neighborhood for the predominant domain
* Total number of enriched domains: the number of domains for which this node's neighborhood has at least 1 significant enrichment (numbers greater than 1 potentially indicate multi-functional nodes)
* Number of enriched attributes per domain: a comma-separated list of numbers, each of which represents a domain (numbered from 2 on) and indicates the number of attributes in that domain for which this node's neighborhood is significantly enriched. For example, "0,0,5,0,15" means that this node's neighborhood is enriched for 5 attributes associated with domain 4 and 15 attributes associated with domain 6 (note: domain ID = 1 is reserved for attributes that are not sufficiently enriched throughout the network, so the domain list typically from 2).
	

EXAMPLES
========

**Note.** Further examples and a step-by-step application of  SAFE to the 2016 genetic interaction network are provided in this manuscript:

> Baryshnikova, A. (2016).  "Spatial Analysis of Functional Enrichment (SAFE) in Large Biological Networks", to be published in "Methods in Molecular Biology", edited by Louise von Stechow and Alberto Santos Delgado. <https://doi.org/10.1101/094904>


Example 1
---------

**Task:** Annotate the genetic interaction similarity network (Costanzo~Boone, 2010) with GO biological process data.

1. Open MATLAB. 

2. Let's assume that the `safe/` folder is located at `/AAA/BBB/`. Add it to the MATLAB path:

	`addpath(genpath('/AAA/BBB/safe/'));`
	
3. Create a folder that would store the SAFE results. For example: 

	`mkdir('/CCC/DDD/');`
		
4. Make a copy of the SAFE settings file `safe.ini` (located in `/AAA/BBB/safe/`) and save it in `/CCC/DDD/`.

5. Leave all settings in `safe.ini` at default values.

6. Open the SAFE main file `/AAA/BBB/safe/safe.m` and set the `path_to_results_folder` variable to the absolute path of the results folder:

	`path_to_results_folder = '/CCC/DDD/';`
	
7. `run('safe.m');`

8. The results will be visually displayed in MATLAB, as well as stored in `/CCC/DDD/`.


Example 2
---------

**Task:** Annotate the genetic interaction similarity network (Costanzo~Boone, 2010) with the quantitative chemical-genomic profile for doxorubicin from Hoepfner~Movva, 2014.

1. Open MATLAB. 

2. Let's assume that the `safe/` folder is located at `/AAA/BBB/`. Add it to the MATLAB path:

	`addpath(genpath('/AAA/BBB/safe/'));`
	
3. Create a folder that would store the SAFE results. For example: 
	
	`mkdir('/CCC/DDD/');`
	
4. Make a copy of the SAFE settings file `safe.ini` (located in `/AAA/BBB/safe/`) and save it in `/CCC/DDD/`.

5. Open the `/CCC/DDD/safe.ini` file. Leave all settings at default values, except for:  

    `annotationfile = /AAA/BBB/safe/data/hoepfner_movva_2014_doxorubicin.txt`  (where `/AAA/BBB/` is the full path to the folder where the SAFE code is stored)
    
	`annotationsign = both`  
	
	`unimodalityType = `
	
6. Open the SAFE main file `/AAA/BBB/safe/safe.m` and set the `path_to_results_folder` variable to the absolute path of the results folder:

	`path_to_results_folder = '/CCC/DDD/';`
	
7. `run('safe.m');`

8. The results will be visually displayed in MATLAB, as well as stored in `/CCC/DDD/`.


Example 3
---------

**Task:** Load a custom network from a text file, apply the Kamada-Kawai (Cytoscape) layout and annotate it with GO biological process data.

1. Launch Cytoscape and leave it open in the background. 

	**Note**: Cytoscape 3.2 or later is required. In Cytoscape 3.2, make sure that the cyREST app is installed and running (in Cytoscape 3.3, the app is automatically included). For more information, see <https://github.com/idekerlab/cyREST/wiki>.

2. Open MATLAB. 

3. Let's assume that the `safe/` folder is located at `/AAA/BBB/`. Add it to the MATLAB path:

	`addpath(genpath('/AAA/BBB/safe/'));`
	
4. Create a folder that would store the SAFE results. For example: 

	`mkdir('/CCC/DDD/');`
	
5. Make a copy of the SAFE settings file `safe.ini` (located in `/AAA/BBB/safe/`) and save it in `/CCC/DDD/`.

6. Open the `/CCC/DDD/safe.ini` file. Leave all settings at default values, except for:

	`networkfile = /AAA/BBB/safe/data/Costanzo_Science_2010_5col.txt` (where `/AAA/BBB/` is the full path to the folder where the SAFE code is stored). Any other network file in the same format can be used -- see **Input file formats** for options.
	
	`layoutAlgorithm = Kamada-Kawai (Cytoscape)`
	
7. Open the SAFE main file `/AAA/BBB/safe/safe.m` and set the `path_to_results_folder` variable to the absolute path of the results folder:

	`path_to_results_folder = '/CCC/DDD/';`
	
8. `run('safe.m');`

9. The results will be visually displayed in MATLAB, as well as stored in `/CCC/DDD/`.


DESCRIPTION OF THE ALGORITHM
============================

All tunable parameters (paths, thresholds, optional steps, etc.) are specified in the `safe.ini` file.


Input data
----------

1. A network `G` that consists of a set of nodes `V` and a set of undirected edges `E`. Edge weights `w` can be binary or quantitative. The network can be provided in one of three formats: an edge list, an adjacency matrix or a Cytoscape session file (see **Input file formats** above). If a network file is not provided, SAFE uses the default genetic interaction similarity network from Costanzo, Baryshnikova, et al., Science, 2010.

2. A functional annotation standard in the form of a matrix `A = L x F`, where `L` are node labels and `F` are functional node attributes (see the `safe.ini` file). The matrix `A` can contain binary or quantitative functional attributes. In case of binary attributes (e.g., Gene Ontology), the entry `a(i,j)` in matrix `A` equals 1 if the i-th node is annotated to the j-th functional attribute, and 0 otherwise. In case of quantitative attributes (e.g., a chemical genomics dataset), the entry `a(i,j)` in matrix `A` equals to the quantitative measurement associated with the i-th node in the j-th experimental condition. Nodes `V` in the network `G` that don't match any node labels `L` in the functional annotation standard `A` are ignored. If a functional annotation file is not provided, SAFE employs the default Gene Ontology biological process annotations for *S. cerevisiae*.


Algorithm
---------

1. Load the network. 

2. *Optional.* If a map of the network is not already available (i.e., if the network is loaded as an edge list or an adjacency matrix) or the user wants to create a new map (i.e., if the existing network map, loaded via a Cytoscape session, should be overwritten), apply the Kamada-Kawai network layout algorithm implemented as part of the MatlabBGL toolbox (<https://github.com/dgleich/matlab-bgl>).

3. Load the functional annotation standard.

4. For every node `v(i)` in the network `G`, define a local neighborhood as the set of nodes `U(i)` that can be reached from `v(i)` by traveling no more than a distance `d`. Distance between any two nodes can be measured as an unweighted, a map-based (default) or an edge weight-based shortest path length between them. By default, `d` equals to the 0.5th-percentile of all pair-wise node distances in the network. Both the distance metric type and the value of `d` can be defined by the user in the `safe.ini` file.

5. Calculate the enrichment of each neighborhood `U(i)` for each of the functional attributes `F(j)` in the annotation standard `A`. Two ways are used to calculate enrichment, depending on whether the attributes are binary or quantitative, as determined automatically by SAFE.  
	5a) For binary attributes, the enrichment is calculated by performing a one-tailed Fisher's exact test. The significance of the enrichment is determined as the probability `p(i,j)` that a random overlap between `U(i)` and `F(j)` will fall in the interval `[S(i,j) Inf)`.  
	5b) For quantitative annotations, the enrichment is calculated using an empirical test. A neighborhood score `S(i,j)` is computed by summing the quantitative attribute values `F(j)` of all nodes in `U(i)`. The score `S(i,j)` is then compared to the mean `m` and the standard deviation `s` of 1,000 random neighborhoods scores obtained by reshuffling the network labels and, thus, the functional attribute values. The significance of the enrichment is determined as the probability that a single observation from a normal distribution with mean `m` and standard deviation `s` will fall in the interval `(-Inf S(i,j)]` or `[S(i,j) Inf)`, depending on whether higher or lower scores are of interest, as defined by the user in the `safe.ini` file.

6. Convert neighborhood significance p-values `p(i,j)` into neighborhood enrichment scores `O(i,j) = -log10(p(i,j))`, normalized to a range from 0 to 1.

7. *Optional.* Restrict the analysis to region-specific functional attributes using one of three available methods (see the `safe.ini` file).  
	7a) Hartigan’s dip test of unimodality: defines a functional attribute as region-specific if the distribution of pair-wise distances between its significantly enriched neighborhoods is unimodal.  
	7b) Radius-based method (default): defines a functional attribute as region-specific if at least 65% of significantly enriched neighborhoods are within a distance of `2d`, where d is the radius of the neighborhood (see #4 above).  
	7c) Subtractive clustering-based method: defines a functional attribute as region-specific if there is at most one cluster center among the significantly enriched neighborhoods.

8. *Optional.* Group functional attributes into functional regions based on the similarity of their enrichment landscapes. Distance between functional attributes `F(j)` and `F(k)` is computed using 1 - Jaccard (default) or Pearson correlation coefficients between their corresponding enrichment landscapes `O(j)` and `O(k)` (see the `safe.ini` file for the choice of the distance metric). Given an `F x F` distance matrix, functional regions are formed using agglomerative hierarchical clustering with average linkage. Clusters are defined by a distance threhold (see the `safe.ini` file), set by default at 75% of the cluster tree height.

9. Assign a random `RGB` color to each functional region. All functional attributes in the same region are assigned the same color.

10. Determine the color `RGB(i)` of every node `v(i)` in the network `G` by computing a weighted average of the colors associated with the functional attributes for which the neighborhood `U(i)` is significantly enriched. The weights correspond to the square of the neighborhood enrichment scores `O(i,j)` for each functional attribute `F(j)`.

11. Output the results.  
	11a) Plot all nodes according to their position on the network and their computed color.  
	11b) Generate an automatic label for each functional region by identifying the five most frequent words in the denominations of the functional attributes that belong to that region.  
	11c) Print all data, including the list of functional regions, their automatically generated labels and the complete list of functional attributes enriched within each region, into text files.

12. Save all data and settings into a SAFE session file.


HELP
====

Please direct all questions/comments to Anastasia Baryshnikova (<abaryshnikova@calicolabs.com>).

The main repository for this code is at <https://bitbucket.org/abarysh/safe>. Please subscribe to the repository to receive live updates about new code releases and bug reports.


HOW TO CITE
==========

The manuscript describing SAFE and its applications is available at:

> Baryshnikova, A. (2016). Systematic Functional Annotation and Visualization of Biological Networks. Cell Systems. <http://doi.org/10.1016/j.cels.2016.04.014>


LICENSING
=========

The MIT License (MIT)

Copyright (c) 2016 Anastasia Baryshnikova

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.